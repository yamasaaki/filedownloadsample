//
//  ViewController.swift
//  FileDownload
//
//  Created by Masaaki Yamada on 2021/01/02.
//

import UIKit

class ViewController: UIViewController {
    let docFilename = "data.txt"
    let tmpFileName = "tmpData.txt"

    @IBOutlet weak var myText: UITextField!
    @IBOutlet weak var tmpText: UITextField!
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /*
     * ファイル保存【保存対象：Document】
     */
    @IBAction func saveButton(_ sender: Any) {
        let text = myText.text
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last {
            let path = dir.appendingPathComponent(docFilename)

            do {
                try text?.write(to: path, atomically: true, encoding: String.Encoding.utf8)
            } catch let error as NSError {
                print("エラー：\(error)")
            }
        }
    }
    
    /*
     * ファイル保存【保存対象：tmp】
     */
    @IBAction func createTextFileTmp(_ sender: Any) {
        print("tmpファイル保存")

        guard let tmpText = tmpText.text else { return }

        // tmpのURL + ファイル名 取得
        let tmpFile = FileManager.default.temporaryDirectory.appendingPathComponent(tmpFileName)
        print("[tmp]tempFile = " + tmpFile.absoluteString)

        // ファイルに書き込み
        do {
            try tmpText.write(to: tmpFile, atomically: false, encoding: .utf8)
        } catch {
            print("Error: \(error)")
        }
    }

    /**
     * tmpからDocumentへファイルを保存する。
     */
    @IBAction func copyTmpToDoc(_ sender: Any) {
        print("copy実行")

        // tmpのURL + ファイル名 取得
        // let tmpFile = FileManager.default.temporaryDirectory.appendingPathComponent(tmpFileName)
        let tmpFile = NSHomeDirectory() + "/tmp" + "/" + tmpFileName
        let docDir = NSHomeDirectory() + "/Documents" + "/" + tmpFileName

        print("[copy]tempFile = " + tmpFile)
        if(ViewController.copy(tmpFile, toPathName: docDir)) {
            print("成功")
        } else {
            print("失敗(T_T)")
        }
    }

    
    func copyDocToDoc() {
        // Documentsフォルダ内でのコピー
        let docFile = NSHomeDirectory() + "/Documents" + "/" + docFilename
        let docFile2 = NSHomeDirectory() + "/Documents" + "/" + docFilename + "2" + ".txt"

        if(ViewController.copy(docFile, toPathName: docFile2)) {
            print("成功")
        } else {
            print("失敗(T_T)")
        }
    }

    /**
     ディレクトリまたはファイルのコピーを行います。

     - Parameter atPathName: コピー元のディレクトリパス名またはファイルパス名
     - Parameter toPathName: コピー先のディレクトリパス名
     - Returns: 処理結果
     */
    class func copy(_ atPathName: String, toPathName: String) -> Bool {
        let fileManager = FileManager.default
        do {
            try fileManager.copyItem(atPath: atPathName, toPath: toPathName)
        } catch {
            print("Error: \(error)")
            return false
        }
        return true
    }
}


